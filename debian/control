Source: ropemacs
Section: devel
Priority: optional
Maintainer: Arnaud Fontaine <arnau@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: cdbs (>= 0.4.90~),
               debhelper-compat (= 10),
               dh-python,
               python (>= 2.6.6-3~)
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/python-team/packages/ropemacs.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/ropemacs
Homepage: https://github.com/python-rope/ropemacs

Package: python-ropemacs
Architecture: all
Depends: ${python:Depends},
         ${misc:Depends},
         pymacs,
         python-rope (>= 0.9.4),
         python-ropemode (>= 0.2)
Description: Emacs mode for Python refactoring
 Rope is a Python library that can be used with several editors and IDEs. It
 provides many refactoring operations as well as forms of code assistance like
 auto-completion and access to documentation. For a complete list of features,
 see the project homepage.
 .
 This package provides the features of the python-rope library through a Emacs
 minor mode (on top of the Python mode) using the pymacs package.
